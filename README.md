# Proyecto de Interfaces de Usuario



```markdown

Integrantes 

Castro Mejia Jonatan Alejandro 314027687
Leyva Castillo Luis Angel 314050577

```

```markdown
Tecnologías utilizadas:

* node js (v14.15.4)
* salis js (1.2.4)
```

## Ejecución 

Clonar el repositorio

Instalar dependencias

```markdown
> npm install
```

Para instalar *sails js* usar el siguiente comando

```markdown
> npm install sails -g
```

## Ejecución del Proyecto con *salis js*

```markdown
cd proyecto-interfaces y ejecutamos el comando: 

> sails lift

Despues nos dirigimos a localhost:1337 y veremos nuestro proyecto.
```

