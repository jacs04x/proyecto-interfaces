/**
 * TestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    prueba: (req, res) => {
        return res.view('pruebas/prueba');
    }

};